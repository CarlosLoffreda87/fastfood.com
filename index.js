const button = document.getElementById("reset");

button.addEventListener("click", resetConfirm);

function resetConfirm(){
    alert("Estás seguro de resetear el formulario?")
}


/* ------------------------------------------EJEMPLO PARA CREAR UN NUEVO BUTTON MANIPULANDO EL DOM!!!! ------------------------------------------*/

/* const padre = document.querySelector(".buttons")//  1. UBICAMOS EL PADRE
console.log(padre)

const newBUtton = document.createElement("button")//  2. CREAMOS UN NUEVO BUTTON MEDIANTE CREATEELEMENT (ESPECIFICANDO QUE ELEMENTO QUEREMOS CREAR)
                               

const text = document.createTextNode("Push")//  3. CREAMOS UN TEXTO MEDIANTE CREATENODETEXT
newBUtton.appendChild(text)//  4. INSERTAMOS TEXT DENTRO DEL NUEVO BUTTON

padre.appendChild(newBUtton)//  .5 INSERTAMOS EL NUEVO BUTTON YA CON TEXTO DENTRO DEL PADRE PARA QUE SE UNA CON SUS HERMANOS  */     

/* ----------------------------------------------------------------------------------------------------------------------------------------------*/


/* const last = document.getElementById("link1")




const newLink = document.createElement("a")
const text = document.createTextNode("Nuestros locales")   ÉSTO ES PARA AÑADIR UN ELEMENTO DONDE UNO QUIERA Y NO SOLAMENTE AL FINAL
newLink.appendChild(text)

last.insertAdjacentElement("beforebegin", newLink) */


/* const element = document.getElementById("link1")
console.log(element)
                                                           ÉSTO ES PARA ELIMINAR UN ELEMENTO
const padre = document.querySelector(".navHeader")
console.log(padre)

padre.removeChild(element) */


/* const padre = document.querySelector(".navHeader")
console.log(padre)

const newLink = document.createElement("a")
const text = document.createTextNode("Locales")



newLink.appendChild(text)


padre.insertAdjacentElement("beforebegin", newLink) */

//ELIMINAR ELEMENTO:  IMPORTANTE ACORDARCE QUE PARA USAR EL REMOVECHILD TENEMOS QUE UBICAR AL PADRE MAS CERCANO DEL ELEMENTO A ELIMINAR

/* const padre = document.querySelector(".nosotros")
console.log(padre)

const hijo = document.querySelector("h3")
console.log(hijo)

padre.removeChild(hijo) */


//REMPLAZAR ELEMENTO:

/* const padre = document.querySelector(".nosotros")


const viejo = document.querySelector("h3")
console.log(viejo)

const nuevo = document.createElement("h6")
const text = document.createTextNode("Nosotros")
nuevo.appendChild(text)
console.log(nuevo)

padre.replaceChild(nuevo, viejo) */




/* TRANSFORMAR LAS COLECCIONES QUE GENERAN QUERYSELECTORALL EN ARRAYS PARA PODER MANIPULARLOS MEJOR

const array = Array.from(document.querySelectorAll("p"))
console.log(array) */



//CREAR NUEVOS ELEMENTOS PERO ESPECIFICANDO SUS PROPIEDADES:


/* const nuevo = document.createElement("a")
nuevo.textContent = "CLIKs"
nuevo.className = "clase"
console.log(nuevo)

nuevo.id = "nuevoId"
nuevo.href = "https://youtube.com"
nuevo.target = "blank"


const padre = document.querySelector(".navHeader")
padre.appendChild(nuevo)
 */


/* const email = document.getElementById("enviar")
console.log(email)

email.addEventListener("click", mensaje)


function mensaje(){
        alert("Holis")
}

/* email.removeEventListener("click", mensaje) */ 


const formulario = document.getElementById("form");


formulario.addEventListener("submit", function(e){
    e.preventDefault()
    const email = document.getElementById("email").value
    console.log(email)
})


const checkboks = document.getElementById("activador")
console.log(checkboks)

checkboks.addEventListener("change", (e) => {
    if(e.target.checked){
        console.log("Solicitud aceptada")
    } else{
        console.log("Aceptar terminos y condiciones....")
    }
}
)

const video = document.getElementById("video")
const play = document.getElementById("play")
const pause = document.getElementById("pause")

play.addEventListener("click", ()=> video.play())

pause.addEventListener("click", ()=> video.pause())




















































